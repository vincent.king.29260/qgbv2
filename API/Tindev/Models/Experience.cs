
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Experience {
    public Experience() {
    }
    public int ID { get; set; }
    //public int UserID { get; set; }
    public string Name { get; set; }    
    public string Description { get; set; }    
    public DateTime BeginDate { get; set; }    
    public DateTime EndDate { get; set; }
    public User User { get; set; }
}