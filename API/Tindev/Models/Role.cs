
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Role {
    public Role() {
    }
    public int ID { get; set; }
    public string Name { get; set; }
    public ICollection<User> Users { get; set; }
    public ICollection<Need> Needs { get; set; }
    public ICollection<Like> Likes { get; set; }
}