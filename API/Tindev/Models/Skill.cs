
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Skill {
    public Skill() {
    }
    public int ID { get; set; }
    //public int UserID { get; set; }
    public int Rate { get; set; }
    public string Name { get; set; }
    public User User { get; set; }
}