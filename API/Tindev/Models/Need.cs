
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Need {
    public Need() {
    }
    public int ID { get; set; }
    /*public int UserID { get; set; }
    public int RoleID { get; set; }
    public int ProjectID { get; set; }*/
    public User Member { get; set; }
    public Role Role { get; set; }
    public Project Project { get; set; }
}