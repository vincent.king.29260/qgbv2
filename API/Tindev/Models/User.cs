
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class User {
    public User() {
    }
    public int ID { get; set; }
    public string Lastname { get; set; }
    public string Firstname { get; set; }
    public string Mail { get; set; }
    public byte[] Password { get; set; }
    public byte[] PasswordSalt { get; internal set; }
    public string Token { get; internal set; }
    public ICollection<Skill> Skills { get; set; }
    public ICollection<Like> Likes { get; set; }
    public ICollection<Need> Needs { get; set; }
    public ICollection<Experience> Experiences { get; set; }
    public ICollection<Project> Projects { get; set; }
}