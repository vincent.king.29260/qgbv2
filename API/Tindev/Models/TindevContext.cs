﻿using Microsoft.EntityFrameworkCore;

namespace Tindev.Models
{
    public class TindevContext : DbContext
    {
        public TindevContext(DbContextOptions<TindevContext> options)
            : base(options)
        {
        }

        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Need> Needs { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Experience>().ToTable("Experience");
            modelBuilder.Entity<Like>().ToTable("Like");
            modelBuilder.Entity<Need>().ToTable("Need");
            modelBuilder.Entity<Project>().ToTable("Project");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<Skill>().ToTable("Skill");
            modelBuilder.Entity<User>().ToTable("User");
        }
    }
}