
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Project {
    public Project() {
    }
    public int ID { get; set; }
    //public int UserID { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime CreationDate { get; set; }
    public User Owner { get; set; }
    public ICollection<Need> Needs { get; set; }
    public ICollection<Like> Likes { get; set; }
}