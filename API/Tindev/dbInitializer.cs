﻿using Tindev.Models;
using System;
using System.Linq;
using Tindev.Services;

namespace Tindev
{
    public static class DbInitializer
    {
        public static void Initialize(TindevContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            var roles = new Role[]
            {
                new Role{Name="Développeur web"},
                new Role{Name="Développeur logiciel"},
                new Role{Name="Graphiste"},
                new Role{Name="Chef de projet"},
                new Role{Name="Administrateur système"},
            };

            foreach (Role r in roles)
            {
                context.Roles.Add(r);
            }
            context.SaveChanges();

            byte[] passwordHash, passwordSalt;
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes("pwd"));
            }
            User user = new User { Firstname = "Jean", Lastname = "Bonneau", Mail = "jean@bonneau.com", Password=passwordHash, PasswordSalt=passwordSalt };
            context.Users.Add(user);
            context.SaveChanges();

            var projects = new Project[]
            {
                new Project { Name="Site commerçant", CreationDate=DateTime.Parse("2020-01-01"), Description="Site de vente de talent pour les développeurs en carton", Owner=user},
                new Project { Name="Site vitrine", CreationDate=DateTime.Parse("2019-12-13"), Description="Site vitrine pour un artisan CAP coiffure", Owner=user},
            };

            foreach (Project p in projects)
            {
                context.Projects.Add(p);
            }
            context.SaveChanges();
        }
    }
}