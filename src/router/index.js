import Vue from 'vue'
import VueRouter from 'vue-router'

import Matchs from "../components/Matchs";
import Projets from "../components/Projets";
import Profil from "../components/Profil";
Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Match',
        component: Matchs
    },
    {
        path: '/Match',
        name: 'Match',
        component: Matchs
    },
    {
      path: '/Projets',
      name: 'Projets',
      component: Projets
    },
    {
      path: '/Profil',
      name: 'Profil',
      component: Profil
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
