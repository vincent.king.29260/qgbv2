import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user:{
      id:'',
      email:"",
      nom:'',
      prenom:'',
      token:'',
    }


  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
