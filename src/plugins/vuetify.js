import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            dark: {
                primary: '#FD2B7A',
                secondary: '#424242',
                accent: '#FF7456',
            }
        }
    },
});
