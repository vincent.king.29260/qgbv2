# Tindev



## Install the application

### Get sources with git from the repository
over SSH `git@gitlab.com:vincent.king.29260/qgbv2.git`

over HTTPS `https://gitlab.com/vincent.king.29260/qgbv2.git`

##Run the application

###Development environnement

#### Install dependances
run `npm install`

####Run the application

* run vue-cli development serveur `npm run serve`
* run api server
    * open `/API/Tindev/Tindev.sln` with **Visual Studio**
    * run `Tindev`


# API

## Authentication

### Request

+ Method : **POST**
+ URI : **api/users/authenticate**
+ Headers : 
   + Content-Type : **application/json**
+ Body :
    + mail
    + password

### Response

 User object with authentication token
 
 ## Authenticated request
 
+ Headers : 
   + Content-Type : **application/json**
   + Authorization : **Bearer _token_**